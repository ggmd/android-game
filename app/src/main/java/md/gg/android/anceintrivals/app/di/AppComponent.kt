package md.gg.android.anceintrivals.app.di

import dagger.Component
import md.gg.android.anceintrivals.data.MapRepository

/**
 * TODO: optionaly add javadocs
 */
@AppScope
@Component(modules = arrayOf(AppModule::class, DataModule::class))
interface AppComponent {
    fun mapRepository(): MapRepository
}
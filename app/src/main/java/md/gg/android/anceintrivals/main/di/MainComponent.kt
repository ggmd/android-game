package md.gg.android.anceintrivals.main.di

import dagger.Component
import md.gg.android.anceintrivals.app.di.AppComponent
import md.gg.android.anceintrivals.main.MainActivity

/**
 * TODO: optionaly add javadocs
 */
@MainScope
@Component(modules = arrayOf(MainModule::class), dependencies = arrayOf(AppComponent::class))
interface MainComponent {
    fun inject(activity: MainActivity)
}
package md.gg.android.anceintrivals.main.di

import dagger.Module
import dagger.Provides
import md.gg.android.anceintrivals.data.MapRepository
import md.gg.android.anceintrivals.main.MainActivity
import md.gg.android.anceintrivals.main.MainPanZoomController
import md.gg.android.anceintrivals.main.MainSceneManager
import md.gg.android.anceintrivals.main.Map3DGenerator
import md.gg.android.anceintrivals.main.mvp.MainModel
import md.gg.android.anceintrivals.main.mvp.MainPresenter
import md.gg.android.anceintrivals.main.mvp.MainView

/**
 * TODO: optionaly add javadocs
 */
@Module
class MainModule(private val activity: MainActivity) {
    @MainScope
    @Provides
    fun model(mapRepository: MapRepository): MainModel {
        return MainModel(mapRepository)
    }

    @MainScope
    @Provides
    fun presenter(model: MainModel, view: MainView): MainPresenter {
        return MainPresenter(model, view)
    }

    @MainScope
    @Provides
    fun view(panZoomController: MainPanZoomController, mainSceneManager: MainSceneManager): MainView {
        return MainView(activity, panZoomController, mainSceneManager)
    }

    @MainScope
    @Provides
    fun mainSceneManager(mapGenerator: Map3DGenerator): MainSceneManager {
        return MainSceneManager(activity, mapGenerator)
    }

    @MainScope
    @Provides
    fun mapGenerator(): Map3DGenerator {
        return Map3DGenerator(activity)
    }

    @MainScope
    @Provides
    fun panZoomController(): MainPanZoomController {
        return MainPanZoomController(activity)
    }
}
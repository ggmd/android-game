package md.gg.android.anceintrivals.main.mvp

import android.view.ViewGroup
import io.reactivex.subjects.BehaviorSubject
import md.gg.android.anceintrivals.data.domain.GameMap
import md.gg.android.anceintrivals.main.MainActivity
import md.gg.android.anceintrivals.main.MainPanZoomController
import md.gg.android.anceintrivals.main.MainSceneManager
import org.rajawali3d.math.vector.Vector2
import org.rajawali3d.surface.IRajawaliSurface
import org.rajawali3d.surface.RajawaliSurfaceView

/**
 * TODO: optionaly add javadocs
 */
class MainView(activity: MainActivity, panZoomController: MainPanZoomController, private val mainSceneManager: MainSceneManager) {
    val dragSubject: BehaviorSubject<Vector2> = BehaviorSubject.create()
    val scaleSubject: BehaviorSubject<Float> = BehaviorSubject.create()

    init {
        val surface = RajawaliSurfaceView(activity)
        surface.apply {
            setFrameRate(60.0)
            renderMode = IRajawaliSurface.RENDERMODE_WHEN_DIRTY
        }

        activity.addContentView(surface, ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT))

        surface.setSurfaceRenderer(mainSceneManager)

        panZoomController.init(surface)

        panZoomController.onPan.subscribe { dragSubject.onNext(it) }
        panZoomController.onZoom.subscribe { scaleSubject.onNext(it) }
    }

    fun setData(map: GameMap) {
        mainSceneManager.onInit.subscribe {
            mainSceneManager.buildMap(map)
        }
    }

    fun pan(distance: Vector2) {
        mainSceneManager.pan(distance)
    }

    fun scale(factor: Double) {
        mainSceneManager.scale(factor)
    }
}
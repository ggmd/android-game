package md.gg.android.anceintrivals.app

import android.app.Application
import md.gg.android.anceintrivals.app.di.AppComponent
import md.gg.android.anceintrivals.app.di.AppModule
import md.gg.android.anceintrivals.app.di.DaggerAppComponent
import timber.log.Timber

class AncientRivalsApp : Application() {
    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())

        appComponent = DaggerAppComponent.builder().appModule(AppModule(this)).build()
    }
}
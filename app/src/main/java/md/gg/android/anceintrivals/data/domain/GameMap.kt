package md.gg.android.anceintrivals.data.domain

import android.graphics.Path
import md.gg.android.anceintrivals.utils.lineTo
import md.gg.android.anceintrivals.utils.moveTo

data class GameMap(
        var provinces: MutableList<Province>,
        var offset: Pair<Double, Double> = Pair(0.0, 0.0)
)

private const val MAP_SCALE = 1f

fun GameMap.terrainConnections(): List<Pair<MapNode, MapNode>> {
    val result: MutableList<Pair<MapNode, MapNode>> = mutableListOf()
    val seaProvinces = provinces.filter { it.type == ProvinceType.SEA }
    provinces.filter { it.type == ProvinceType.TERRAIN }.forEach {
        it.mapConnections.forEach {
            var byTheSea = false
            for (province in seaProvinces) {
                provinceLoop@
                for (seaConnection in province.mapConnections) {
                    if (connectionsAreEqual(it, seaConnection)) {
                        byTheSea = true
                        break@provinceLoop
                    }
                }
            }
            if (!result.contains(it) && !byTheSea) {
                result.add(it)
            }
        }
    }
    return result
}

fun GameMap.prepare() {
    provinces.forEach {
        it.mapConnections.forEach {
            it.toList().forEach {
                it.x -= offset.first
                it.y -= offset.second

                it.x *= MAP_SCALE
                it.y *= MAP_SCALE
            }
        }
    }
}

fun provincePath(province: Province, canvasOffset: Pair<Int, Int>): Path {
    val path = Path()
    val nodes = province.nodes()
    val first = nodes.first()

    var (offX, offY) = canvasOffset.copy()
    offX /= 2
    offY /= 2

    path.moveTo(first.x + offX, first.y + offY)
    for (i in 1..nodes.size - 1) {
        path.lineTo(nodes[i].x + offX, nodes[i].y + offY)
    }
    path.lineTo(first.x + offX, first.y + offY)
    return path
}
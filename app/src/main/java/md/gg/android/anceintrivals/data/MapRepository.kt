package md.gg.android.anceintrivals.data

import android.content.Context
import com.github.salomonbrys.kotson.fromJson
import com.google.gson.Gson
import md.gg.android.anceintrivals.R
import md.gg.android.anceintrivals.data.domain.GameMap

class MapRepository(
        private val context: Context,
        private val gson: Gson
) {
    companion object {
        private const val MAP_FILE_ID = R.raw.map
    }

    fun loadFromRawRes(): GameMap {
        val resource = context.resources.openRawResource(R.raw.map)
        val json = resource.bufferedReader().use { it.readText() }
        return gson.fromJson(json)
    }
}
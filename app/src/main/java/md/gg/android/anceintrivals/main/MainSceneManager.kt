package md.gg.android.anceintrivals.main

import android.content.Context
import android.view.MotionEvent
import io.reactivex.subjects.BehaviorSubject
import md.gg.android.anceintrivals.data.domain.GameMap
import md.gg.android.anceintrivals.utils.plus
import org.rajawali3d.lights.DirectionalLight
import org.rajawali3d.math.vector.Vector2
import org.rajawali3d.math.vector.Vector3
import org.rajawali3d.renderer.RajawaliRenderer

/**
 * TODO: optionaly add javadocs
 */
class MainSceneManager(context: Context, private val mapGenerator: Map3DGenerator) : RajawaliRenderer(context) {
    private lateinit var dirLight: DirectionalLight

    val onInit: BehaviorSubject<Unit> = BehaviorSubject.create()

    override fun onOffsetsChanged(xOffset: Float, yOffset: Float, xOffsetStep: Float, yOffsetStep: Float, xPixelOffset: Int, yPixelOffset: Int) {
        // no-op
    }

    override fun onTouchEvent(event: MotionEvent?) {
        // no-op
    }

    override fun initScene() {
        dirLight = DirectionalLight(1.0, .2, -1.0)
        dirLight.apply {
            setColor(1f, 1f, 1f)
            power = 1f
        }

        currentScene.addLight(dirLight)

        currentCamera.apply {
            z = 500.0
            y = 500.0
            farPlane = 2000.0
            rotate(Vector3.Axis.X, 75.0)
        }

        onInit.onNext(Unit)
    }

    fun pan(distance: Vector2) {
        currentCamera.position.plus(distance)
    }

    fun scale(factor: Double) {
        currentCamera.y += (factor - 1.0) * 10.0
    }

    fun buildMap(map: GameMap) {
        mapGenerator.generate(map)
        currentScene.addChildren(mapGenerator.objects)
    }
}
package md.gg.android.anceintrivals.main

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import md.gg.android.anceintrivals.R
import md.gg.android.anceintrivals.app.AncientRivalsApp
import md.gg.android.anceintrivals.main.di.DaggerMainComponent
import md.gg.android.anceintrivals.main.di.MainModule
import md.gg.android.anceintrivals.main.mvp.MainPresenter
import md.gg.android.anceintrivals.main.mvp.MainView
import javax.inject.Inject

class MainActivity : AppCompatActivity() {
    @Inject
    lateinit var presenter: MainPresenter
    @Inject
    lateinit var view: MainView

    private lateinit var mainSceneManager: MainSceneManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setup()
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    private fun setup() {
        DaggerMainComponent.builder()
                .appComponent((application as AncientRivalsApp).appComponent)
                .mainModule(MainModule(this)).build().inject(this)
    }
}

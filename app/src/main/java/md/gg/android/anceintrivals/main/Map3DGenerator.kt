package md.gg.android.anceintrivals.main

import android.content.Context
import android.graphics.*
import com.github.czyzby.noise4j.map.Grid
import com.github.czyzby.noise4j.map.generator.noise.NoiseGenerator
import md.gg.android.anceintrivals.R
import md.gg.android.anceintrivals.data.domain.*
import md.gg.android.anceintrivals.utils.repeat
import org.rajawali3d.Object3D
import org.rajawali3d.materials.Material
import org.rajawali3d.materials.methods.DiffuseMethod
import org.rajawali3d.materials.textures.NormalMapTexture
import org.rajawali3d.materials.textures.Texture
import org.rajawali3d.terrain.SquareTerrain
import org.rajawali3d.terrain.TerrainGenerator


/**
 * TODO: optionaly add javadocs
 */
class Map3DGenerator(private val context: Context) {
    private var map: GameMap? = null

    val objects: MutableList<Object3D> = mutableListOf()

    fun generate(map: GameMap) {
        this.map = map

        val texture = createMapTexture()
        createSimpleGround(texture)
    }

    private fun createMapTexture(): Texture {
        val edges = map?.terrainConnections()
        val bw = map?.offset?.first?.times(2)
        val bh = map?.offset?.second?.times(2)
        val config = Bitmap.Config.ARGB_8888
        val bitmap = Bitmap.createBitmap(bw?.toInt()!!, bh?.toInt()!!, config)
        val canvas = Canvas(bitmap)
        val paint = Paint().apply {
            color = Color.argb(255, 0, 0, 0)
            style = Paint.Style.FILL
        }
        val fullRect = Rect(0, 0, canvas.width, canvas.height)
        canvas.drawRect(fullRect, paint)
        paint.color = Color.argb(255, 255, 255, 255)
        paint.strokeWidth = 6.0f
        if (edges != null) {
            for ((first, second) in edges) {
                canvas.drawLine(first.x.toFloat() + canvas.width / 2, first.y.toFloat() + canvas.height / 2,
                        second.x.toFloat() + canvas.width / 2, second.y.toFloat() + canvas.height / 2, paint)
            }
        }

        drawTerrainNames(canvas)

        val texture = Texture("map_info", bitmap)
        return texture
    }

    private fun drawTerrainNames(canvas: Canvas) {
        val paint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
            textSize = 24.0f
            color = Color.argb(255, 255, 255, 255)
        }
        map?.provinces?.filter {
            it.type == ProvinceType.TERRAIN
        }?.forEach {
            val position = it.placeName()
            val w = paint.measureText(it.name)
            canvas.drawText(it.name, position.x.toFloat() + canvas.width / 2 - w / 2, position.y.toFloat() + canvas.height / 2, paint)
        }
    }

    private fun createSimpleGround(mapTexture: Texture) {
        val terrainMaterial = Material().apply {
            enableLighting(true)
            useVertexColors(true)
            diffuseMethod = DiffuseMethod.Lambert()
        }

        val groundBitmap = BitmapFactory.decodeResource(context.resources, R.drawable.ground)
        val groundTexture = Texture("ground", groundBitmap.repeat(4.0, 4.0))
        val groundNorBitmap = BitmapFactory.decodeResource(context.resources, R.drawable.groundnor)
        val normalTexture = NormalMapTexture("groundnor", groundNorBitmap.repeat(4.0, 4.0))
        terrainMaterial.addTexture(mapTexture)
        terrainMaterial.addTexture(groundTexture)
        terrainMaterial.addTexture(normalTexture)

        val bitmap = createHeightBitmap()

        val terrainParams = SquareTerrain.createParameters(bitmap).apply {
            setScale(map?.offset?.first!! * 2 / 128.0, 128.0, map?.offset?.second!! * 2 / 128.0)
            divisions = 128
            textureMult = 1.0
            basecolor = Color.argb(255, 5, 8, 18)
            middleColor = Color.argb(255, 32, 48, 24)
            upColor = Color.argb(255, 7, 12, 4)
        }
        val terrain = TerrainGenerator.createSquareTerrainFromBitmap(terrainParams, true).apply {
            material = terrainMaterial
        }

        objects.add(terrain)
    }

    private fun createHeightBitmap(): Bitmap {
        val bw = map?.offset?.first?.times(2)
        val bh = map?.offset?.second?.times(2)
        val config = Bitmap.Config.ARGB_8888
        val bitmap = Bitmap.createBitmap(128, 128, config)

        applyNoise(bitmap)
        val targetBitmap = Bitmap.createScaledBitmap(bitmap, bw?.toInt()!!, bh?.toInt()!!, true)
        val canvas = Canvas(targetBitmap)
        createPlaceForSeas(canvas, bw.toInt(), bh.toInt())

        return targetBitmap
    }

    private fun applyNoise(bitmap: Bitmap) {
        val noiseGenerator = NoiseGenerator()
        val grid = Grid(128)
        noiseStage(grid, noiseGenerator, 4, 196f)
        val pixels = IntArray(bitmap.width * bitmap.height)
        for (i in 0..bitmap.width - 1) {
            for (j in 0..bitmap.height - 1) {
                val cell = grid.get(i, j)
                val intCell = cell.toInt()
                pixels[j * bitmap.width + i] = Color.argb(255, intCell, intCell, intCell)
            }
        }
        bitmap.setPixels(pixels, 0, bitmap.width, 0, 0, bitmap.width, bitmap.height)
    }

    private fun noiseStage(grid: Grid, noiseGenerator: NoiseGenerator, radius: Int, modifier: Float) {
        noiseGenerator.apply {
            this.radius = radius
            this.modifier = modifier
            generate(grid)
        }
    }

    private fun createPlaceForSeas(canvas: Canvas, bw: Int, bh: Int) {
        val paint = Paint().apply {
            style = Paint.Style.FILL
        }

        paint.color = Color.BLACK
        map?.provinces?.filter { it.type == ProvinceType.SEA }!!
                .map { provincePath(it, Pair(bw, bh)) }
                .forEach { canvas.drawPath(it, paint) }
    }
}

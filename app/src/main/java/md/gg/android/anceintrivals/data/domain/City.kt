package md.gg.android.anceintrivals.data.domain

import java.io.Serializable

data class City(var name: String = "") : Serializable
package md.gg.android.anceintrivals.main.mvp

import md.gg.android.anceintrivals.data.MapRepository
import md.gg.android.anceintrivals.data.domain.GameMap
import md.gg.android.anceintrivals.data.domain.prepare

class MainModel(private val mapRepository: MapRepository) {
    fun getMap(): GameMap {
        val map = mapRepository.loadFromRawRes()
        map.prepare()
        return map
    }
}
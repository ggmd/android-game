package md.gg.android.anceintrivals.app.di

import android.content.Context
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import md.gg.android.anceintrivals.data.MapRepository

/**
 * TODO: optionaly add javadocs
 */
@Module
class DataModule {
    @AppScope
    @Provides
    fun gson(): Gson {
        return GsonBuilder().create()
    }

    @AppScope
    @Provides
    fun mapRepository(context: Context, gson: Gson): MapRepository {
        return MapRepository(context, gson)
    }
}
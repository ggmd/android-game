package md.gg.android.anceintrivals.app.di

import android.content.Context
import dagger.Module
import dagger.Provides

/**
 * TODO: optionaly add javadocs
 */
@Module
class AppModule(val context: Context) {
    @AppScope
    @Provides
    fun context(): Context {
        return context
    }
}